import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.io.FileWriter;
import java.io.FileReader;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;
import javax.mail.internet.InternetAddress;


import javax.mail.*;



public class ConnectMail{

      static String content = "";
      static int limit = 1000; //how many emails to go through
      static ArrayList<String> munchList =  new ArrayList<String>();
     //Alogirthm #1 runs in O(n)
    public static void fetch(String pop3Host, String storeType, String user,
    String password) { 
      
    try {
       // create properties field
       Properties properties = new Properties();

       //create email session with given password
      Session emailSession = Session.getInstance(properties,
      new javax.mail.Authenticator() {
         protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(
               user, password);
         }
      });

       // connect with the imaps server
       Store store = emailSession.getStore("imaps");

       store.connect(pop3Host, user, password);

       // create the folder object and open it
       Folder emailFolder = store.getFolder("INBOX");
       emailFolder.open(Folder.READ_ONLY);

       BufferedReader reader = new BufferedReader(new InputStreamReader(
        System.in));

       // retrieve the messages from the folder in an array and print it

       Message[] messages = emailFolder.getMessages();
       System.out.println("messages.length---" + messages.length);

       //array to store content of email
       String[] messageArr = new String[limit];
       Date[] messageDates = new Date[limit];
       String[] sender = new String[limit];
       String[] subject = new String[limit];
       int index = 0;

       try{
       for (int i = messages.length-1; i < messages.length; i--) {
          Message message = messages[i];
          messageDates[index] = message.getSentDate();
          messageArr[index] = writePart(message);
          sender[index] = InternetAddress.toString(messages[i].getFrom());
          subject[index] = message.getSubject();
          index++;
          if(index == limit){ //how many recent messages you want to collect
                break;
          }
          
       }
      } catch (ArrayIndexOutOfBoundsException e){
            
      }

       // close the store and folder objects
       emailFolder.close(false);
       store.close();


      ArrayList<String> SquareSpaceEmails = filterEmails(messageArr, sender, subject);
      ArrayList<Integer> emailLocations = findEmailsIndex(subject);

      //check to make sure lists are the same size
      if(SquareSpaceEmails.size() != emailLocations.size()){
            System.out.println("SquareSpaceEmails and EmailLocations are not the same!!");
            System.out.println("SquareSpaceEmails: " + SquareSpaceEmails.size() + "\nEmailLocations : " + emailLocations.size());
            System.exit(0);
      } else{
            System.out.println("SquareSpaceEmails and EmailLocations sizes are the same!");
      }

      for(int i = 0; i < emailLocations.size(); i++){
           getOrderInfo(SquareSpaceEmails, i, messageDates);   
      }
      System.out.println("\n");
      

   } catch (NoSuchProviderException e) {
      e.printStackTrace();
   } catch (MessagingException e) {
      e.printStackTrace();
   } catch (IOException e) {
      e.printStackTrace();
   } catch (Exception e) {
      e.printStackTrace();
   } 
 
   
 }

 public static String writePart(Part p) throws Exception {
       //will return the content of the email
      
    System.out.println("----------------------------");
    System.out.println("CONTENT-TYPE: " + p.getContentType());

    //check if the content is plain text
    if (p.isMimeType("text/plain")) {
      System.out.println("This is plain text");
      System.out.println("---------------------------");
       content = (String) p.getContent();
    } 
    //check if the content has attachment
    else if (p.isMimeType("multipart/*")) {
      System.out.println("This is a Multipart");
      System.out.println("---------------------------");
       Multipart mp = (Multipart) p.getContent();
       int count = mp.getCount();
       for (int i = 0; i < count; i++)
          writePart(mp.getBodyPart(i));
    } 
    //check if the content is a nested message
    else if (p.isMimeType("message/rfc822")) {
      System.out.println("This is a Nested Message");
      System.out.println("---------------------------");
       writePart((Part) p.getContent());
    } 
    else {
       Object o = p.getContent();
       if (o instanceof String) {
          System.out.println("This is a string");
          System.out.println("---------------------------");
       } 
       else if (o instanceof InputStream) {
          System.out.println("This is just an input stream");
          System.out.println("---------------------------");
          InputStream is = (InputStream) o;
          is = (InputStream) o;

       } 
       else {
          System.out.println("This is an unknown type");
          System.out.println("---------------------------");
       }
    
            
 }
 return content;
}


public static ArrayList<String> filterEmails(String[] messageArr, String[] sender, String[] subject){      
      ArrayList<String> filteredEmails = new ArrayList<String>();
      for(int i = 0; i < sender.length; i++){
            if(sender[i].equals("Squarespace <no-reply@squarespace.com>") && subject[i].contains("munchYum: A New Order has Arrived ")){
                  //if index of SquareSpace is found
                  //get index from messageArr to filteredList
                  filteredEmails.add(messageArr[i]);
            }      
      }
      return filteredEmails;
}

public static ArrayList<Integer> findEmailsIndex(String[] subject){
      ArrayList<Integer> keyIndexs = new ArrayList<Integer>();
      //find all the correct indexs of the emails
      for(int i = 0; i < subject.length; i++){
            if(subject[i].contains("munchYum: A New Order has Arrived ")){
                  keyIndexs.add(i);
            }
      }
      return keyIndexs;
}


 public static void getOrderInfo(ArrayList<String> messageList, int i, Date[] datesArr){
       //get the oder summary from the string 
       //split the string into an array and perform a binary search on it
       String[] order = messageList.get(i).split("Order Summary",2);
       String tempOrder = order[1];
       String[] fullOrder = tempOrder.split("Additional Information",2);
       String orderSummary = fullOrder[0];
       
      System.out.println("++++++++++++++++++++++++++++++++++\n" + orderSummary);
      //get the relevent info from orderSummary through a scanner
      Scanner scan = new Scanner(orderSummary);
      for(int j = 0; j < 7; j++){
            scan.nextLine();
      }
      boolean moreOrders = true;
      String item = scan.nextLine();
     
     while (moreOrders){
            //scan the next 4 lines to search for the quantity
            String QTY = "1";   //default
            
            for(int j = 0; j < 4; j++){
                  String buffer = scan.nextLine();
                  Pattern pattern = Pattern.compile(".*[^0-9].*");
                  Matcher matcher = pattern.matcher(buffer);
                  if(matcher.matches()){
                        QTY = buffer;
                  }
            }

            item += "[" + QTY;
            
            // //item is added to the order
            munchList.add(item + "[" + datesArr[i]);
            System.out.println("ITEM::::: " + item);
            // //scan five more lines to see if there is another item
            String backupItem = "";
            for(int j = 0; j < 5; j++){
                  backupItem = scan.nextLine();
            }

            String lastLine = scan.nextLine();
            try{
                  if(lastLine.charAt(1) == ('Q')){
                        lastLine =  backupItem;
                  }
            } catch (StringIndexOutOfBoundsException e){
                  System.out.println("No line here");
            }
           
            System.out.println("Last Line::::: " + lastLine);
            Pattern pattern = Pattern.compile(".*[a-zA-Z]+.*");
            Matcher matcher = pattern.matcher(lastLine);

            if (matcher.matches()) {
                  moreOrders = true;
                  item = lastLine;
             } else {
                  moreOrders = false;
             }
      }
      scan.close();

      }
      
public static void createMunchYumItem(String item, FileWriter writer){
            //break item into product name and quanitity
            String[] itemSplit = item.split("\\[", 3);
            item = itemSplit[0];
            String quantity = itemSplit[1];
            String date = itemSplit[2];
            double shipping = 1.79;

            //reformat the data
            date = getDate(date);

            MunchYumOrder MY_item = new MunchYumOrder(item);
            double p = MY_item.getPrice();
            String price = Double.toString(p);

            //check if there is comma in the item name and remove it
            int commaLocation = item.indexOf(',');
            if(commaLocation != -1){
                  //a comman exists in the item name
                  //remove the command and rewrite item
                  StringBuilder sb = new StringBuilder(item);
                  sb.deleteCharAt(commaLocation);
                  item = sb.toString();
            }
            
            //write info to CVS file
            try{
                  writer.append(item);
                  writer.append(',');
                  writer.append(quantity);
                  writer.append(',');
                  writer.append(price);
                  writer.append(',');
                  writer.append(date);
                  writer.append('\n');

            } catch (IOException e){
                  e.printStackTrace();
            } 

      }

public static String getDate(String fullDate){
      //split the date into month and numerical date
      String[] dateSplit = fullDate.split(" ", 4);
      String month = dateSplit[1];
      String num = dateSplit[2];

      String date = month + " " + num;
      return date;
}

public static void dailyProfit(String csv, FileWriter writer) throws NumberFormatException{
      BufferedReader br = null;
      String line = "";
      String del = ",";
      ArrayList<String> dates = new ArrayList<String>();
      DecimalFormat df2 = new DecimalFormat(".##");


      //read all written dates from the file
      try{
            br = new BufferedReader(new FileReader(csv));
            while((line = br.readLine()) != null){
                  
                  String[] info = line.split(del);
                  dates.add(info[3]); //fill a list with all the dates
            }

            br.close();
      } catch (IOException e){
            System.out.println("File was not found");
      } catch (ArrayIndexOutOfBoundsException ex){

      }
      
      //create a set to get all the unquie dates
      Set<String> unquieDates = new HashSet<String>(dates);
      unquieDates.remove("Received On");  //not a date
      Double[] sums = new Double[unquieDates.size()];

      //fill the array with 0s
      for(int i = 0; i < unquieDates.size(); i++){
            sums[i] = 0.0;
      }

      try{
            br = new BufferedReader(new FileReader(csv));
            while((line = br.readLine()) != null){
                  
                  String[] info = line.split(del);
                  int i = 0;
                  for(String date : unquieDates){
                        if(date.equals(info[3])){
                              //the the column contains the date add it it to thse array
                              double dub = Double.parseDouble(info[2]);

                              //multiply dub by Quantity
                              String formatQTY = info[1].substring(1);
                              int quant = Integer.parseInt(formatQTY);

                              dub *= quant;
                              //add dubs to current amount in [i]
                              dub += sums[i];
                              //update [i]
                              sums[i] = dub;
                        }
                        i++;
                  }
            }

            br.close();
      } catch (IOException e){
            System.out.println("File was not found");
      } catch (ArrayIndexOutOfBoundsException ex){

      }
      
      //write all the unquie dates to the first row
      
      try{
            //move down two full lines
            System.out.println("Writing dates to file...");
            //reset i to 0
            int i = 0;
            for(String date : unquieDates){
                  writer.append(date);
                  writer.append(',');

                  //format double to 2 decimal places
                  df2.format(sums[i]);

                  String sum = Double.toString(sums[i]);
                  writer.append(sum);
                  writer.append('\n');
                  i++;
            }
           writer.flush();

      } catch (IOException ex){
            System.out.println("File not found");
      } 

}


 public static void main(String[] args) {

      String host = "imap.gmail.com";// change accordingly
      String mailStoreType = "pop3";
      String username = " chenf@allegheny.edu";// change accordingly
      String password = "emporiobobthomas";// change accordingly
      String csvFile = "/home/race/CMPSC250/250-finalproject/transaction.csv";

      //Call method fetch
      fetch(host, mailStoreType, username, password);    
      //set the get the price to be added to the csv file
      //need to set the top coloumns of the File Writer
      FileWriter writer = null;
      try{
            writer = new FileWriter(csvFile);
            writer.append("Item");
            writer.append(',');
            writer.append("Quantity");
            writer.append(',');
            writer.append("Price");
            writer.append(',');
            writer.append("Received On");
            writer.append('\n');

            //loop through all items and extract data from index to the file
            for(int i = 0; i < munchList.size(); i++){
                createMunchYumItem(munchList.get(i), writer);
            }
            writer.append('\n');
            writer.append('\n');
            writer.append("All Dates");
            writer.append(',');
            writer.append("Total Profit");
            writer.append('\n');

      } catch (IOException e){
            e.printStackTrace();
      } finally {
            try{
                  writer.flush();
            } catch (IOException ex){
                  ex.printStackTrace();
            }
      }

     dailyProfit(csvFile, writer);
      
 }

}