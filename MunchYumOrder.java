import java.util.*;

public class MunchYumOrder{

    private String item;
    private static double price;
    private static double shipping = 1.79;
    static HashMap<String, Double> hmap = new HashMap<String, Double>();

    public MunchYumOrder(){}

    public MunchYumOrder(String item){
        this.item = item;

        hmap.put("Cape Cod Chips", 4.49);
        hmap.put("Doritos Nacho Cheese (Party)", 5.49); 
        hmap.put("Doritos Nacho Cheese", 0.95); 
        hmap.put("Doritos Nacho Cheese ", 0.95);//werird duplicate 
        hmap.put("Doritos Cool Ranch", 5.49);
        hmap.put("Cheez-Its Original", 3.99);
        hmap.put("Skittles Original", 3.49); //raised the price
        hmap.put("Oreo", 4.19);
        hmap.put("Oreo Birthday Cake", 3.99);
        hmap.put("Flamin' Hot Cheetos", 3.99);
        hmap.put("Goldfish Baked", 3.79);
        hmap.put("Rice Krispies", 0.79);
        hmap.put("Pop-Tarts Smores", 1.79);
        hmap.put("Pop-Tarts Cookies & Cream", 1.59); //lowered the price
        hmap.put("Ben and Jerry's | Half Baked", 5.49);
        hmap.put("Pop-Tars Chocolate Fudge ", 1.49);
        hmap.put("Fruit Roll-Ups", 0.89);
        hmap.put("Ben and Jerry's | The Tonight Dough", 5.29);
        hmap.put("Gushers", 0.69);
        hmap.put("Clif Bar",1.89);
        hmap.put("Totino's Pizza Rolls", 3.19);
        hmap.put("Chef Boyardee Ravioli", 1.99);
        hmap.put("Halo Top | Chocolate Chip Cookie Dough", 5.49);
        hmap.put("Tositos Scoops!", 5.49);
        hmap.put("Halo Top | Cookies and Cream",5.49);
        hmap.put("Halo Top | Cookie Dough",5.49);
        hmap.put("Kraft Mac & Cheese",1.99);
        hmap.put("Ben and Jerry's | Peanut Butter Cup", 4.99);
        hmap.put("Tositos Chunky Salsa", 3.99);
        hmap.put("Cheetos Crunchy",3.99);
        hmap.put("Ben and Jerry's | Cookie Dough", 5.49);
        hmap.put("Ben and Jerry's | Chocolate Fudge Brownie", 4.99);
        hmap.put("Pringles Variety Pack | 3 count", 2.99);
        hmap.put("Reese's", 1.29);
        hmap.put("Edwards Cookies and Crème Pie", 10.49);
        hmap.put("Cheddar Fries", 3.99);
        hmap.put("Chocolate Dipped Vanilla Cones", 9.99);
        hmap.put("Starbucks Mocha Frap", 2.39);
        hmap.put("Starbucks Vanilla Frap", 2.39);
        hmap.put("Starbucks Caramel Frap", 2.99);
        hmap.put("Monster Energy, Original", 1.99);
        hmap.put("Gatorade, Grape", 1.79);
        hmap.put("Gatorade, Glacier Freeze", 1.79);
        hmap.put("Gatorade, Fruit Punch", 1.79);
        hmap.put("Gatorade, Lemon-Lime", 1.79);
        hmap.put("Gatorade, Orange", 1.79);
        hmap.put("Gatorade, Cool Blue", 1.79);
        hmap.put("Monster Engery, Lo-Carb", 2.19);
        hmap.put("Red Bull", 2.99);
        hmap.put("Starbucks Coffee Frap",2.19);
        hmap.put("Dunkin' Donuts K-Cups",8.99);
        hmap.put("Tide Detergent", 6.99);
        hmap.put("Dunkin' Donuts Ground Coffee", 7.49);
        hmap.put("Starbucks K-Cups", 10.29);
        hmap.put("Charmin Ultra Strong Toilet Paper", 6.99);

    }

    public void setItem(String item){
        this.item = item;
    }

    public String toString(){
        return item;
    }

    public double getPrice(){
        //O(1) time, so technially an algorithm?
        if(hmap.containsKey(item)){
            price = hmap.get(item);
            return price;
        } else {
            System.out.println(item + "does not exist in our records");
            return 0.0;
        }
    }

    //  public static int binarySearch(String[] email, String value, int min, int max){
//        //using the sorted array of keywords perform a binary search to find 
//        //the index of the 
//       if(min > max){
//             return -1;
//       }

//       int mid = (min + max) / 2;
 
//       //see if the keyword is 
// 	if(email[mid].equals(value)){
//             return mid;
//       }else if(keywordsArr[mid].compareTo(value) < 0){
//             return binarySearch(keywordsArr, value, mid + 1, max);
//       }else {
//             return binarySearch(keywordsArr, value, min, mid - 1);
//       }
//  }
}